import boto3
import subprocess
import json
import csv
import tarfile
import os
import sys
import trade_detail_extract as ex
import utils

osTempFolder = "/tmp"
s3 = boto3.client('s3')
comprehend = boto3.client('comprehend')
dynamo = boto3.resource('dynamodb')

def lambda_handler(event, context):
    
    #Get the Bucket Name and File Key from the event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    file_key_path = file_key.split('/')
    csv_reference_file = ('/').join(file_key_path[0:len(file_key_path) - 3]) + ".csv"
    json_output_file = ('/').join(file_key_path[0:len(file_key_path) - 3]) + ".jsonl"
    confirms_output_path = ('/').join(file_key_path[0:len(file_key_path) - 3])
    
    print("Bucket Name: " + bucket_name)
    print("File Key: " + file_key)
    print("Reference CSV File: " + csv_reference_file)
    print("Ouput JSONL File: " + json_output_file)
    print("Confirms Output Path: " + confirms_output_path)

    #Create a directory to work in
    processing_folder = osTempFolder + '/processing/'
    if not os.path.exists(os.path.dirname(processing_folder)):
        os.makedirs(os.path.dirname(processing_folder))
    
    #Download the files from S3
    s3.download_file(bucket_name, file_key, processing_folder + 'output.tar.gz')
    s3.download_file(bucket_name, csv_reference_file, processing_folder + 'reference.csv')
    
    #Extract the file to the /tmp/processing directory
    tar = tarfile.open(processing_folder + 'output.tar.gz', "r:gz")
    tar.extractall(path=processing_folder)
    tar.close()
    
    #Read the jsonl file from the output
    with open(processing_folder + 'predictions.jsonl') as json_data:
        result = [json.loads(jline) for jline in json_data ]
    preds = [ utils.clean_line(l, 1) for l in result ]
    
    #Read the CSV reference file
    csv_data = []
    with open(processing_folder + 'reference.csv') as f:
        for row in csv.DictReader(f):
            if "0" in row['Trade_Detected']:
                csv_data.append(row)
    
    #Merge the data together and apply classification filters and extraction logic
    for i in range(len(csv_data)):
        csv_data[i].update(preds[i])

        # if csv_data[i]['pred'] != 'NONTRADE':
        #     print("--------------------------------------------------------------------------")
        #     print(csv_data[i]['pred'], '  :::::  ', csv_data[i]['Message'])
        
        #Apply multi-leg identification for trade confirms
        if csv_data[i]['pred'] != 'NONTRADE':
            csv_data[i]['pred_multileg'] = ex.get_multileg(csv_data[i]['Message'])
            csv_data[i]['pred_pipeline'] = ex.get_pipeline(csv_data[i]['Message'])
            csv_data[i]['pred_buysell'] = ex.get_buy_sell(csv_data[i]['Message'])
            
            #Extract details for EFP single-leg trade confirms
            if csv_data[i]['pred_multileg'] == 0 and csv_data[i]['pred'] == 'EFP':
                csv_data[i]['pred_trade_details'] = ex.get_efp_details(csv_data[i]['Message'])
    
            #Extract details for FLOATING EFP single-leg trade confirms
            if csv_data[i]['pred_multileg'] == 0 and csv_data[i]['pred'] == 'FLOATING EFP':
                csv_data[i]['pred_trade_details'] = ex.get_floating_efp_details(csv_data[i]['Message'])
                
    # Print identified trades ...
    # printed = 0
    # for i in range(len(csv_data)):
    #     if csv_data[i]['pred'] in ['EFP', 'FLOATING EFP']:
    #         print("--------------------------------------------------------------------------")
    #         for j in csv_data[i].keys():
    #             if j == 'pred_trade_details':
    #                 for k in csv_data[i][j].keys():
    #                     if k == 'pred_price':
    #                         for p in csv_data[i][j][k].keys():
    #                             print(k, " - ", p, " - ", csv_data[i][j][k][p])
    #                     else:
    #                         print(k, " - ", csv_data[i][j][k])
    #             else:
    #                 print(j, " - ", csv_data[i][j])
    #         print(" ")
    #         printed += 1
    #     if printed > 1:
    #         break
    
    # Insert identified trades into Dynamo ...
    for i in range(len(csv_data)):
        if csv_data[i]['pred'] != 'NONTRADE':
            utils.tradeDynamoPut(dynamo, csv_data[i])

    # Save all messages and content in a json file
    with open('/tmp/out.jsonl', 'w') as outfile:
        for i in range(len(csv_data)):
            outfile.write(json.dumps(csv_data[i])+'\n')
    s3.upload_file('/tmp/out.jsonl', 'tradecapture-classifier-output', json_output_file)
        
    # Save trade confirms (as predicted by classifier) to txt file for entity extraction and csv file to get details later
    with open('/tmp/confirms.txt', 'w') as outfile1, open('/tmp/confirms_details.csv', 'w') as outfile2:
        csv_writer = csv.writer(outfile2, delimiter=',')
        csv_writer.writerow(['Time', 'UserName', 'Message'])
    
        for i in range(len(csv_data)):
            if csv_data[i]['pred'] != 'NONTRADE':
                # print(csv_data[i]['Message'])
                outfile1.write(csv_data[i]['Message']+'\n')
                # outfile2.write(csv_data[i]['Message'])
                csv_writer.writerow([csv_data[i]['Time'], csv_data[i]['UserName'], csv_data[i]['Message']])

    s3.upload_file('/tmp/confirms.txt', 'tradecapture-entity-input', confirms_output_path+"_confirms.txt")
    s3.upload_file('/tmp/confirms_details.csv', 'tradecapture-entity-input', confirms_output_path+"_confirms_details.csv")


    # Score entity extraction model - VOLUME
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/volume/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradcapture-volume-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting VOLUME recognizer ... ')
        raise e

    # Score entity extraction model - PRODUCT
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/product/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradecapture-product-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting PROUCT recognizer ... ')
        raise e
        
    # Score entity extraction model - CYCLE
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/cycle/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradecapture-cycle-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting CYCLE recognizer ... ')
        raise e

    # Score entity extraction model - PIPELINE
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/pipeline/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradecapture-pipeline-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting PIPELINE recognizer ... ')
        raise e
    
    # Score entity extraction model - CP-TRADER
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/cp-trader/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradecapture-cp-trader-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting CP-TRADER recognizer ... ')
        raise e

    # Score entity extraction model - CP-COMPANY
    try:
        start_response = comprehend.start_entities_detection_job(
            InputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"_confirms.txt",
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': 's3://tradecapture-entity-input/'+confirms_output_path+"/cp-company/"
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            EntityRecognizerArn='arn:aws:comprehend:us-east-2:952151468489:entity-recognizer/tradecapture-cp-company-v1-1',
            LanguageCode='en'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error starting CP-COMPANY recognizer ... ')
        raise e


    #Clean up the processing folder
    subprocess.call('rm -rf ' + processing_folder, shell=True)
    
    return {
        'statusCode': 200
    }