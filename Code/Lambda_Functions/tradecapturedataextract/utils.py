# utility functions for message processing

from datetime import datetime
import boto3
import os
from boto3.dynamodb.conditions import Key, Attr
import json
import decimal


def clean_line(pred, nontrade_adjust):
    """Reads in a single line of json output from the comprehend classifier and returns formatted dict of scores 
    """
    for i in pred['Classes']:
        if i['Name'] == 'NONTRADE':
            i['Score'] = i['Score'] * nontrade_adjust
    scores = { p['Name'].replace(' ', '_')+'_score':p['Score'] for p in pred['Classes'] }
    scores['pred'] = sorted(pred['Classes'], key=lambda x: x['Score'], reverse=True)[0]['Name']
    return scores


def clean_list_of_one(l):
    """For lists of one element, return only the value to keep presention clean in UI
    """
    if len(l) == 0:
        return 'NA'
    elif len(l) == 1:
        return l[0]
    else:
        return l
        
             
def addToDynamo(dynamodb, version, header, sort, data):
    """Inserts a record into the dynamo database
    """
    table = dynamodb.Table('tradecapture-poc')
    table.put_item(
            Item={
                'trader': header,
                'version_time': "v" + str(version) + "_" + sort,
                'trade_details': data
            }
        )


def tradeDynamoPut(dynamodb, jsonData):
    """Formats a record to insert into dynamo database, including a check for existing record
    """
    
    #Set the table to use for the work. Coming in from environment variables
    table = dynamodb.Table('tradecapture-poc')
    
    #Build the partition and sort keys
    partitionKey = str(jsonData['UserName'])
    sortKey = str(jsonData['Time'])
    
    #Check if the item exists already, and any historical edits
    response = table.query(
        KeyConditionExpression=Key('trader').eq(partitionKey) & Key('version_time').between("v00_" + sortKey, "v99_" + sortKey)
    )
    items = response['Items']
    
    print()
    print(json.loads(json.dumps(jsonData), parse_float=decimal.Decimal))
    print()

    addToDynamo(dynamodb, "00", partitionKey, sortKey, json.loads(json.dumps(jsonData), parse_float=decimal.Decimal))

    #If there are no items returned, we can insert the first version with version number 00
    # if not items:
    #     try:
    #         addToDynamo(dynamodb, "00", partitionKey, sortKey, json.loads(json.dumps(jsonData), parse_float=decimal.Decimal))
    #     except:
    #         print("First:::: ", "00", partitionKey, sortKey, jsonData)
    #         exit

    # #If there are items returned, we need to update version 00 to be the newest edit, however we need audit capability
    # #Copy the data from the existing version 00 and add it to a new item for audit trail
    # else:
    #     versions = []
    #     vZeroItem = {}
    #     for item in items:
    #         versionNumber = item['version_time'].split('_')[0].strip('v')
    #         print('versionNumber :::: ', versionNumber)
    #         versions.append(versionNumber)
    #         if versionNumber == "00":
    #             vZeroItem = item
    #     versions.sort()
    #     if len(versions) < 9:
    #         newVersion = "0" + str(len(versions))
    #     else:
    #         newVersion = str(len(versions))

    #     print(vZeroItem)
    #     print("Upddated:::: ", newVersion, partitionKey, sortKey, vZeroItem['data'])
    #     addToDynamo(dynamodb, newVersion, partitionKey, sortKey, vZeroItem['data'])
    #     addToDynamo(dynamodb, "00", partitionKey, sortKey, json.loads(json.dumps(jsonData), parse_float=decimal.Decimal))   
