import re
import utils

def get_multileg(msg):
    """Identifies multi-leg trades from trade confirmation message.  
    Returns 1 for multi-leg, 0 for single-leg.
    """
    if len(re.findall('buy[s]{0,1} |sell[s]{0,1} |give|take', msg.lower())) > 1 \
            or len(re.findall('cash', msg.lower())) > 1:
        return 1
    else:
        return 0


def get_pipeline(msg):
    """Identifies pielines called out in message
    """
    pipelines = []
    lower = msg.lower()
    
    if len(re.findall('cpl|colonial|bama|non al', lower)) >= 1:
        pipelines.append('Colonial')
    
    if len(re.findall('expl', msg.lower())) >= 1:
        pipelines.append('Explorer')
    
    if len(re.findall('magellan|mag south|mag s', lower)) >= 1:
        pipelines.append('Magellan')
    
    if len(re.findall('buck|bpl', lower)) >= 1:
        pipelines.append('Buckeye')

    if not pipelines:
        pipelines.append('NA')
    
    return ', '.join(sorted(pipelines))


def get_buy_sell(msg):
    lower = msg.lower()
    legs = re.findall('buy[s]{0,1} |sell[s]{0,1} |give|take', msg.lower())
    if len(legs) > 1:
        return "MULTI-LEG"
    else:
        try:
            first_p = re.search('p66|ph66|phillips', msg.lower()).span()[0]
        except:
            first_p = -9
        try:
            first_b = re.search('buy[s]{0,1}|take', msg.lower()).span()[0]
        except:
            first_b = -9
        try:
            first_s = re.search('sell[s]{0,1}|give', msg.lower()).span()[0]
        except:
            first_s = -9
        if first_p >= 0:
            if first_b > 0 and first_s > 0:
                if first_b > first_s:
                    if first_p < first_b:
                        if first_p < first_s:
                            return 'SELL'
                        else:
                            return 'BUY'
                    else:
                        if first_p < first_b:
                            return 'BUY'
                        else:
                            return 'SELL'
            elif first_b > 0:
                if first_p < first_b:
                    return 'BUY'
                else:
                    return 'SELL'
            elif first_s > 0:
                if first_p < first_s:
                    return 'SELL'
                else:
                    return 'BUY'
            else:
                return 'NA'
        else:
            return 'NA'


def get_efp_details(msg): 
    """Extracts trade details, specifically for Single-Leg EFP trades
    """
    lower = str(msg).lower()
    details = {}

    # this only captures product data seen in limited training data ... need to make more comprehensive
    details['pred_product'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall(' a[1-5] | d[1-5] | m[1-5] | h[1-5] | v[1-5] |62gr', lower) ]
    details['pred_quantity'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('\[?[ ]{0,1}\d{1,4}[ ]{0,1}mb[ ]{0,1}\]?', lower) ]
    details['pred_cycles'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('\[?[ ]{0,1}c\d{1,2}\]?', lower) ]
    
    details['pred_product'] = utils.clean_list_of_one(details['pred_product'])
    details['pred_quantity'] = utils.clean_list_of_one(details['pred_quantity'])
    details['pred_cycles'] = utils.clean_list_of_one(details['pred_cycles'])
    
    def get_counterparty(msg):
        """helper function to identify counterparty and trader
        """
        s = re.search('sells\]?[ ]{0,1}\[?to\]?|buys\]?[ ]{0,1}\[?from\]?', msg)
        if not s:
            s = re.search('sells[ ]{0,1}|buys[ ]{0,1}', msg)
        if s:
            msg2 = msg[s.end():]
            counter = msg2[:msg2.find(':')]
            s2 = re.search('\[?[ ]{0,1}\d{1,4}[ ]{0,1}mb[ ]{0,1}\]?| a[1-5] | d[1-5] | m[1-5] | h[1-5] | v[1-5] |62gr|\[?[ ]{0,1}c\d{1,2}\]?', counter)

            if s2:    
                return re.sub('[\[\]]', '', counter[:s2.span()[0]].strip())
            else:
                return re.sub('[\[\]]', '', counter.strip())
        else:
            return 'NA'

    details['pred_counterparty'] = get_counterparty(lower)
    if details['pred_counterparty'] == '':
        details['pred_counterparty'] = 'NA'

    def get_price(msg):
        """helper function to extract pricing related details"""
        lower = msg.lower()
        
        # first get relevant portion of message to extract price from
        c = re.compile('cash|merc|\d{1,2}.\d{4}|minus|plus|diff|nymex|\d{3,4}')
        start = min([ m.start() for m in re.finditer(c, lower) ]) - 20
        end = max([ m.end() for m in re.finditer(c, lower) ]) + 20
        context = lower[start:end]

        if len(context) == 0:
            context='NA'
        
        contract_month = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec', context) ]
        contract_exchange = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('nymex', context) ]
        contract_type = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('ho|rb|rbc|rbob', context) ] 
        
        # if 'merc' and/or 'cash' in context, assume relative value follows
        if re.search('merc', context):
            start_price = re.search('merc', context).span()[1]
            try:
                merc_price = re.search('\d{1,2}.\d+', context[start_price:]).group()
            except:
                merc_price = 'NA'
        else:
            merc_price = 'NA'
            
        if re.search('cash', context):
            start_cash = re.search('cash', context).span()[1]
            try:
                cash_price = re.search('\d{1,2}.\d+', context[start_cash:]).group()
            except:
                cash_price = 'NA'
        else:
            cash_price = 'NA'
        
        # look for differential in points
        d = re.search('[ ]\[?-?\d{3,4}[ \]]', context)
        if d:
            diff = d.group()
            # if 'minus' in text, but no minus sign in front of diff
            if not re.search('-', diff):
                if re.search('minus', context[:d.end()]):
                    diff = '-'+diff.strip()
        else:
            diff = 'NA'
        
        return {'Context': context,
                'Contract_Month': utils.clean_list_of_one(contract_month),
                'Contract_Exch': utils.clean_list_of_one(contract_exchange),
                'Contract_Type': utils.clean_list_of_one(contract_type),
                'Merc_Price': merc_price,
                'Cash_Price': cash_price,
                'Differential': diff}

    details['pred_price'] = get_price(lower)
    
    return details


def get_floating_efp_details(msg): 
    """Extracts trade details, specifically for Single-Leg FLOATING EFP trades
    """
    lower = str(msg).lower()
    details = {}

    # this only captures product data seen in limited training data ... need to make more comprehensive
    details['pred_product'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall(' a[1-5] | d[1-5] | m[1-5] | h[1-5] | v[1-5] |62gr', lower) ]
    details['pred_quantity'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('\[?[ ]{0,1}\d{1,4}[ ]{0,1}mb[ ]{0,1}\]?', lower) ]
    details['pred_cycles'] = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('\[?[ ]{0,1}c\d{1,2}\]?', lower) ]
    
    details['pred_product'] = utils.clean_list_of_one(details['pred_product'])
    details['pred_quantity'] = utils.clean_list_of_one(details['pred_quantity'])
    details['pred_cycles'] = utils.clean_list_of_one(details['pred_cycles'])

    def get_counterparty(msg):
        """helper function to identify counterparty and trader
        """
        s = re.search('sells\]?[ ]{0,1}\[?to\]?|buys\]?[ ]{0,1}\[?from\]?', msg)
        if not s:
            s = re.search('sells[ ]{0,1}|buys[ ]{0,1}', msg)
        if s:
            msg2 = msg[s.end():]
            counter = msg2[:msg2.find(':')]
            s2 = re.search('\[?[ ]{0,1}\d{1,4}[ ]{0,1}mb[ ]{0,1}\]?| a[1-5] | d[1-5] | m[1-5] | h[1-5] | v[1-5] |62gr|\[?[ ]{0,1}c\d{1,2}\]?', counter)

            if s2:    
                return re.sub('[\[\]]', '', counter[:s2.span()[0]].strip())
            else:
                return re.sub('[\[\]]', '', counter.strip())
        else:
            return 'NA'

    details['pred_counterparty'] = get_counterparty(lower)
    if details['pred_counterparty'] == '':
        details['pred_counterparty'] = 'NA'

    def get_price(msg):
        """helper function to extract pricing related details"""
        lower = msg.lower()
        
        # first get relevant portion of message to extract price from
        c = re.compile('settle|\d{1,2}.\d{4}|minus|plus|diff|nymex|\d{3,4}')
        start = min([ m.start() for m in re.finditer(c, lower) ]) - 20
        end = max([ m.end() for m in re.finditer(c, lower) ]) + 20
        context = lower[start:end]
        
        contract_month = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec', context) ]
        contract_exchange = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('nymex', context) ]
        contract_type = [ re.sub('[\[\]]', '', s).strip() for s in re.findall('ho|rb|rbc|rbob', context) ] 
        
        settle_date = 'TBD'
        
        # look for differential in points
        d = re.search('[ ]\[?-?\d{3,4}[ \]]', context)
        if d:
            diff = d.group()
            # if 'minus' in text, but no minus sign in front of diff
            if not re.search('-', diff):
                if re.search('minus', context[:d.end()]):
                    diff = '-'+diff.strip()
        else:
            diff = 'NA'
        
        return {'Context': context,
                'Contract_Month': utils.clean_list_of_one(contract_month),
                'Contract_Exch': utils.clean_list_of_one(contract_exchange),
                'Contract_Type': utils.clean_list_of_one(contract_type),
                'Settle_Date': settle_date,
                'Differential': diff}

    details['pred_price'] = get_price(lower)
    
    return details