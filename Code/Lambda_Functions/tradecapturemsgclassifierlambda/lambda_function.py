import json
import urllib.parse
import boto3

s3 = boto3.client('s3')
comprehend = boto3.client('comprehend')

def lambda_handler(event, context):

    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    if key.find('/') > 0:
        path = key[::-1][key[::-1].find('/'):][::-1]
        guid = key[::-1][key[::-1].find('.')+1:key[::-1].find('/')][::-1]
        s3uri = 's3://'+bucket+'/'+path+guid+'/'
    else:
        path = 'NONE'
        guid = key[:key.find('.')]
        s3uri = 's3://'+bucket+'/'+guid+'/'

    print("BUCKET: ", bucket)
    print("KEY: ", key)
    print("PATH: ", path)
    print("GUID: ", guid)
    print("S3URI: ", s3uri)

    try:
        start_response = comprehend.start_document_classification_job(
            InputDataConfig={
                'S3Uri': 's3://'+bucket+'/'+key,
                'InputFormat': 'ONE_DOC_PER_LINE'
            },
            OutputDataConfig={
                'S3Uri': s3uri
            },
            DataAccessRoleArn='arn:aws:iam::952151468489:role/tradecapture-msg-classifier-data-access',
            DocumentClassifierArn='arn:aws:comprehend:us-east-2:952151468489:document-classifier/tradecapture-msg-classifier-v6'
        )
        
        print("Start response: %s\n", start_response)
        
    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e