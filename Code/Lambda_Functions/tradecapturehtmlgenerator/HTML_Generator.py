from __future__ import print_function # Python 2/3 compatibility
from jinja2 import Environment, FileSystemLoader
import os
import string
import json
from datetime import datetime
import boto3
from boto3.dynamodb.conditions import Key, Attr
import subprocess
import shutil
import errno
import glob
 
root = os.path.dirname(os.path.abspath(__file__))
templates_dir = os.path.join(root, 'templates')
env = Environment( loader = FileSystemLoader(templates_dir) )
template = env.get_template('index.html')
templateTrade = env.get_template('trade.html')
dynamodb = boto3.resource('dynamodb')
osTempFolder = "/tmp"
s3 = boto3.client('s3')

def extract_time(json):
    try:
        # Also convert to int since update_time will be string.  When comparing
        # strings, "10" is smaller than "2".
        testing = int(datetime.strptime(json['trade_details']['Time'], "%Y-%m-%dT%H:%M:%S.%f%z").timestamp())
        return int(datetime.strptime(json['trade_details']['Time'], "%Y-%m-%dT%H:%M:%S.%f%z").timestamp())
    except KeyError:
        return 0

def copy_folder_path(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            print('Directory not copied. Error: %s' % e)

def lambda_handler(event, context):
    table = dynamodb.Table(os.environ['dynamoTable'])
    
    tradeList = []
    for traderUserName in json.loads(os.environ['traderUserNames']):
        response = table.query(
            KeyConditionExpression=Key('trader').eq(str(traderUserName)) & Key('version_time').begins_with("v00_")
        )
        #items = response['Items']
        tradeList += response['Items']
    
    
    #Create a directory to work in
    processing_folder = osTempFolder + '/processing/'
    if not os.path.exists(os.path.dirname(processing_folder)):
        os.makedirs(os.path.dirname(processing_folder))
    
    copy_folder_path("./html", processing_folder + "html")
    copy_folder_path("./static", processing_folder + "static")
    copy_folder_path("./templates", processing_folder + "templates")
    
    filename = os.path.join(processing_folder, 'html', 'index.html')
    with open(filename, 'w', encoding="utf-8") as fh:
        colonialTrades = []
        nonColonialTrades = []
        for trade in tradeList:
            trade_id = trade['trader'] + "_" + trade['version_time'].replace('#','_').replace(':','').replace('.','')
            trade['trade_id'] = trade_id
            colonialTrades.append(trade)
        colonialTrades.sort(key=extract_time, reverse=True)
        fh.write(template.render(
            colonialItems = colonialTrades
        ))
    
    for trade in tradeList:
        if not os.path.exists(os.path.join(processing_folder, 'html', 'trades')):
            os.makedirs(os.path.join(processing_folder, 'html', 'trades'))
        trade_id = trade['trader'] + "_" + trade['version_time'].replace('#','_').replace(':','').replace('.','')
        trade['trade_id'] = trade_id
        tradeFileName = os.path.join(processing_folder, 'html', 'trades', trade_id + ".html")
        with open(tradeFileName, 'w', encoding='utf-8') as fhTrades:
            fhTrades.write(templateTrade.render(
                tradeItem = trade
            ))
    
    shutil.make_archive(os.path.join(processing_folder, 'html'), 'zip', os.path.join(processing_folder, 'html'))
    # files = glob.glob(processing_folder + '/html/**', recursive=True)        
    # for file in files:
    #     if os.path.isfile(file):
    #         pathSplit = file.split('/')
    #         s3Path = pathSplit[-2] + "/" + pathSplit[-1]
    #         s3.upload_file(file, os.environ['s3Bucket'], os.environ['s3SubFolder'] + s3Path.replace("html/",''))
        
    s3.upload_file(os.path.join(processing_folder, 'html.zip'), os.environ['s3Bucket'], os.environ['s3SubFolder'] + "TradeCapture-Viewer.zip")
        
    #Clean up the processing folder
    subprocess.call('rm -rf ' + processing_folder, shell=True)