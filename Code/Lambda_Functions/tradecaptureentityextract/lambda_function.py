import json
import boto3
import subprocess
import json
import csv
import tarfile
import os
import sys
import utils

osTempFolder = "/tmp"
s3 = boto3.client('s3')

def lambda_handler(event, context):
    
    #Get the Bucket Name and File Key from the event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    file_key_path = file_key.split('/')
    csv_reference_file = ('/').join(file_key_path[0:len(file_key_path) - 4]) + "_confirms_details.csv"
    csv_output_file = ('/').join(file_key_path[0:len(file_key_path) - 5]) + "/" + file_key_path[-4] + "_messages_with_entities.csv"
    # confirms_output_path = ('/').join(file_key_path[0:len(file_key_path) - 3])
    
    print("Bucket Name: " + bucket_name)
    print("File Key: " + file_key)
    print(file_key_path)
    print("Reference CSV File: " + csv_reference_file)
    print("Ouput CSV File: " + csv_output_file)
    # print("Confirms Output Path: " + confirms_output_path)

    #Create a directory to work in
    processing_folder = osTempFolder + '/processing/'
    if not os.path.exists(os.path.dirname(processing_folder)):
        os.makedirs(os.path.dirname(processing_folder))
    
    #Download the files from S3
    s3.download_file(bucket_name, file_key, processing_folder + 'output.tar.gz')
    s3.download_file(bucket_name, csv_reference_file, processing_folder + 'reference.csv')
    
    #Extract the file to the /tmp/processing directory
    tar = tarfile.open(processing_folder + 'output.tar.gz', "r:gz")
    tar.extractall(path=processing_folder)
    tar.close()
    
    #Read the jsonl file from the output
    with open(processing_folder + 'output') as json_data:
        entities = [ utils.get_entities(json.loads(jline)) for jline in json_data ]
    
    #Read the CSV reference file
    csv_data = []
    with open(processing_folder + 'reference.csv') as f:
        for row in csv.DictReader(f):
            csv_data.append(row)
    
    #Merge together reference data and entities
    for i in range(len(csv_data)):
        csv_data[i].update(entities[i])
        # print(csv_data[i])
        # print()
        
    #Write combined file back to folder for safe keeping
    with open('/tmp/messages_with_entities.csv', 'w') as outfile:
        csv_writer = csv.writer(outfile, delimiter=',')
        csv_writer.writerow(['Time', 'UserName', 'Message', 'EntityType', 'Entities'])
    
        for i in range(len(csv_data)):
            csv_writer.writerow([csv_data[i]['Time'], 
                                 csv_data[i]['UserName'], 
                                 csv_data[i]['Message'],
                                 csv_data[i]['Entity']['Type'],
                                 csv_data[i]['Entity']['Values']])

    s3.upload_file('/tmp/messages_with_entities.csv', 'tradecapture-entity-input', csv_output_file)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
