

def get_entities(entity_json):
    if len(entity_json['Entities']) == 0:
        return  { 'Entity': { 'Type': 'NA',  'Values': 'NA' }}
    else:
        entities = [ ent['Text'] for ent in entity_json['Entities'] ]
        entity_type = entity_json['Entities'][0]['Type']
        if len(entities) == 1:
            return { 'Entity': { 'Type': entity_type, 'Values': entities[0] }}
        else:
            return { 'Entity': { 'Type': entity_type,  'Values': entities }}
