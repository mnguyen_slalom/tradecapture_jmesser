from __future__ import print_function # Python 2/3 compatibility
import boto3
import subprocess
import json
import os
import uuid
import re
from datetime import datetime, timedelta

#Create the Boto3 S3 client
s3 = boto3.client('s3')
resource = boto3.resource('s3')
osTempFolder = "/tmp"
num = re.compile('\d')

def download_dir(client, resource, dist, local, bucket):
    paginator = client.get_paginator('list_objects')
    for result in paginator.paginate(Bucket=bucket, Delimiter='/', Prefix=dist):
        if result.get('CommonPrefixes') is not None:
            for subdir in result.get('CommonPrefixes'):
                download_dir(client, resource, subdir.get('Prefix'), local, bucket)
        for file in result.get('Contents', []):
            dest_pathname = os.path.join(local, file.get('Key'))
            if not os.path.exists(os.path.dirname(dest_pathname)):
                os.makedirs(os.path.dirname(dest_pathname))
            if file.get('Key').endswith('.gz'):
                resource.meta.client.download_file(bucket, file.get('Key'), dest_pathname)
                
def trade_identifier_base_check(message):
    phillipsNamesToCheck = ['phillips', 'philips', 'p66', 'p 66']
    tradeTermsToCheck = ['buy', 'sell', 'give', 'take']
    if any(name in message.lower() for name in phillipsNamesToCheck):
        if any(term in message.lower() for term in tradeTermsToCheck):
            return True
            #print(message.lower() + "\n")
        else:
            return False         
            
def message_filter(msg):
    if "Instant messages" in str(msg) \
            or len(msg) < 50 \
            or len(msg) > 700 \
            or not bool(re.search(num, str(msg))):
        return False
    else:
        return True 

def function_handler(event, context):
    #s3Bucket = event['s3Bucket']
    s3Bucket = os.environ['messages_bucket']
    #kinesisFolder = event['kinesisFolder']
    kinesisFolder = os.environ['kinesis_folder_name']
    eventDate = datetime.strptime(str(event['time']), "%Y-%m-%dT%H:%M:%S%z")
    #yearToDownload = event['yearToDownload']
    #monthToDownload = event['monthToDownload']
    #dayToDownload = event['dayToDownload']
    yearToDownload = str(eventDate.year)
    monthToDownload = str(f"{eventDate:%m}")
    dayToDownload = str(f"{eventDate:%d}")
    if not os.path.exists(os.path.dirname(osTempFolder + '/logs/')):
        os.makedirs(os.path.dirname(osTempFolder + '/logs/'))
    dateString = yearToDownload + "/" + monthToDownload + "/" + dayToDownload
    dateStringDashed = dateString.replace("/","-")
    download_dir(s3, resource, kinesisFolder + "/" + dateString + "/", '/tmp/logs', s3Bucket)
    if not os.path.exists(os.path.dirname(osTempFolder + '/logs/unzipped/')):
        os.makedirs(os.path.dirname(osTempFolder + '/logs/unzipped/'))
    gunzipCommand = ["find", "/tmp/logs", "-type", "f", "-name", "*.gz", "-exec", "zcat", "{}", ';' ]
    with open(osTempFolder + "/logs/unzipped/output.txt", "w") as out:
        subprocess.run(gunzipCommand, stdout=out, check=True)
    uuidName = str(uuid.uuid4().hex)
    with open(osTempFolder + "/logs/unzipped/output.txt") as f:
        with open(osTempFolder + "/logs/unzipped/output.csv", "a") as csvOutput:
            with open(osTempFolder + "/logs/unzipped/detected_trade_messages.txt", "a") as detectedTradeOutput:
                csvOutput.write("Time,UserName,Contact_DisplayName,Message,Sent/Received,Contact_ID,Contact_UserName,Contact_Service,Trade_Detected,Reference_Line_Number\n")
                foundTrades = 0
                for line in f:
                    cleanedUpLine = line.replace("\'", "\"")
                    jsonMessage = json.loads(cleanedUpLine)
                    sentOrReceived = "Sent"
                    if "chatMessage" in jsonMessage['eventName']:
                        if bool(jsonMessage['payload']['isIncomingMessage']):
                            sentOrReceived = "Received"
                        isTradeFound = message_filter(jsonMessage['payload']['message'].replace(",",";").replace('\n', ' ').replace('\r', ''))
                        if isTradeFound:
                            csvOutput.write(jsonMessage['payload']['time']+','+jsonMessage['payload']['username']+','+jsonMessage['payload']['contactDisplayName']+','+jsonMessage['payload']['message'].replace(",",";").replace('\n', ' ').replace('\r', '')+','+sentOrReceived+','+jsonMessage['payload']['contactId']+','+jsonMessage['payload']['contactUsername']+','+jsonMessage['payload']['contactService']+','+'0'+','+str(foundTrades)+'\n')
                            detectedTradeOutput.write(jsonMessage['payload']['message'].replace(",",";").replace('\n', ' ').replace('\r', '')+'\n')
                            foundTrades += 1
                        else:
                            csvOutput.write(jsonMessage['payload']['time']+','+jsonMessage['payload']['username']+','+jsonMessage['payload']['contactDisplayName']+','+jsonMessage['payload']['message'].replace(",",";").replace('\n', ' ').replace('\r', '')+','+sentOrReceived+','+jsonMessage['payload']['contactId']+','+jsonMessage['payload']['contactUsername']+','+jsonMessage['payload']['contactService']+','+'1'+','+'-'+'\n')
    #s3.upload_file(osTempFolder + "/logs/unzipped/output.csv", s3Bucket, kinesisFolder+ "/" + dateString + "/" + uuidName + ".csv")
    #s3.upload_file(osTempFolder + "/logs/unzipped/detected_trade_messages.txt", s3Bucket, kinesisFolder+ "/" + dateString + "/" + uuidName + ".txt")
    s3.upload_file(osTempFolder + "/logs/unzipped/output.csv", os.environ['comprehend_bucket'], dateString + "/" + uuidName + ".csv")
    s3.upload_file(osTempFolder + "/logs/unzipped/detected_trade_messages.txt", os.environ['comprehend_bucket'], dateString + "/" + uuidName + ".txt")
    subprocess.call('rm -rf /tmp/logs', shell=True)
    return "OK"