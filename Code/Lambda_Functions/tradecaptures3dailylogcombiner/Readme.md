# tradecaptures3dailylogcombiner

This Lambda function is designed to be run daily. It will download all of the files collected from the Kinesis Firehose pipeline and stored in to S3 based on the year, month, and day. The file will produce a result of two files which contain chat messages and whether they should be fed into the Comprehend classification model.

## Input
1. Kinesis Firehose output files (*.gz) from day

## Output
1. {GUID}.csv
2. {GUID.txt