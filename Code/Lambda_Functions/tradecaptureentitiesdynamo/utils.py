import json
import decimal
import re
from botocore.exceptions import ClientError

# Helper class to convert a DynamoDB item to JSON.
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)
        
        
def tradeDynamoUpdate(dynamodb, jsonData):
    """Formats a record to insert into dynamo database, including a check for existing record
    """
    
    #Set the table to use for the work. Coming in from environment variables
    table = dynamodb.Table('tradecapture-poc')
    
    #Build the partition and sort keys
    partitionKey = str(jsonData['UserName'])
    sortKey = str(jsonData['Time'])
    
    data = remove_from_dict(jsonData, ['UserName', 'Time', 'Message'])
    # json.loads(json.dumps(data), parse_float=decimal.Decimal)
    
    # print(partitionKey, sortKey, data)
    
    
    UpdateExpression = [ "trade_details."+d+" = :val"+str(i) for i, d in enumerate(data) ]
    ExpressionAttributeValues = { ":val"+str(i): data[d] for i, d in enumerate(data) }
    
    response = table.update_item(
        Key={
            'trader': partitionKey,
            'version_time': "v" + str("00") + "_" + sortKey
        },
        UpdateExpression='set '+','.join(UpdateExpression),
        ExpressionAttributeValues=ExpressionAttributeValues,
        ReturnValues="UPDATED_NEW"
    )

    # response = table.get_item(
    #     Key={
    #         'trader': partitionKey,
    #         'version_time': "v" + str("00") + "_" + sortKey
    #     }
    # )
    
    # try:
    #     response = table.get_item(
    #         Key={
    #             'trader': partitionKey,
    #             'version_time': "v" + str("00") + "_" + sortKey
    #         }
    #     )
    # except ClientError as e:
    #     print(e.response['Error']['Message'])
    # else:
    #     item = response['Item']
    #     print("GetItem succeeded:")
    #     current_data = item['trade_details'] #dict(json.dumps(item, indent=4, cls=DecimalEncoder))
    #     current_data.update(data)
        
    #     print(current_data)
    #     print()
        
    #     response = table.update_item(
    #         Key={
    #             'trader': partitionKey,
    #             'version_time': "v" + str("00") + "_" + sortKey
    #         },
    #         UpdateExpression='SET trade_details = :val0',
    #         ExpressionAttributeValues={ ':val0': current_data },
    #         ReturnValues="UPDATED_NEW"
    #     )

def remove_from_dict(dict_obj, elements):
    new_dict = dict_obj.copy()
    for e in elements:
        try:
            del new_dict[e]
        except KeyError:
            pass
    return new_dict
    
def array_from_string(string):
    if string[0] == '[' and string[-1] == ']':
        return [ re.sub("\'? ?", '', val) for val in string[1:-1].split(',') ]
    else:
        return string