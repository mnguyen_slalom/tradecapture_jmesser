import json
import boto3
import os
import csv
import json
import decimal
import utils

osTempFolder = "/tmp"
s3 = boto3.client('s3')
dynamo = boto3.resource('dynamodb')
lambda_client = boto3.client('lambda')

def lambda_handler(event, context):
    
    #Get the Bucket Name and File Key from the event
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    file_key = event['Records'][0]['s3']['object']['key']
    file_key_path = file_key.split('/')
    
    print("Bucket Name: " + bucket_name)
    print("File Key: " + file_key)

    #Create a directory to work in
    processing_folder = osTempFolder + '/processing/'
    if not os.path.exists(os.path.dirname(processing_folder)):
        os.makedirs(os.path.dirname(processing_folder))
    
    path = ('/').join(file_key_path[0:len(file_key_path) - 1])
    names = ["cp-company", "cp-trader", "volume", "cycle", "product", "pipeline"]
    required_files = [ path + "/" + ent + "_messages_with_entities.csv" for ent in names ]
    
    existing_files = [ key['Key'] for key in s3.list_objects(Bucket=bucket_name, Prefix=path)['Contents'] ]

    #Check to ensure all 6 entity file are available
    if set(required_files).issubset(existing_files):
        for fname in required_files:
            s3.download_file(bucket_name, fname, processing_folder + fname.split('/')[-1])
    
        for f in existing_files:
            if 'confirms_details' in f:
                reference_file = f
    
        s3.download_file(bucket_name, reference_file, processing_folder + "reference_file.csv")
    
        csv_data = []
        with open(processing_folder + "reference_file.csv") as ref:
            for row in csv.DictReader(ref):
                csv_data.append(row)
            
        def entity_updater(fname):        
            with open(processing_folder + fname) as f:
                for i, row in enumerate(csv.DictReader(f)):
                    if row['EntityType'] != 'NA':
                        csv_data[i].update({row['EntityType']: utils.array_from_string(row['Entities'])})
                        
        entity_updater('cp-company_messages_with_entities.csv')
        entity_updater('cp-trader_messages_with_entities.csv')
        entity_updater('product_messages_with_entities.csv')
        entity_updater('volume_messages_with_entities.csv')
        entity_updater('cycle_messages_with_entities.csv')
        entity_updater('pipeline_messages_with_entities.csv')
        
        for i in range(len(csv_data)):
            # print(json.loads(json.dumps(csv_data[i]), parse_float=decimal.Decimal))
            utils.tradeDynamoUpdate(dynamo, csv_data[i])
        
        lambda_client.invoke(FunctionName="arn:aws:lambda:us-east-2:952151468489:function:tradecapture-html-generator", InvocationType='Event')
        
        return {
            'statusCode': 200,
            'body': json.dumps('All entity files available - combined and updated dynamo')
        }

    else:
        return {
            'statusCode': 200,
            'body': json.dumps('Not all entity files are available .... ')
        }