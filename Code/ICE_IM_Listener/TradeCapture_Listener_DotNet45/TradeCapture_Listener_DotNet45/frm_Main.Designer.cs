﻿namespace TradeCapture_Listener
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Main));
            this.lbl_Records_Sent = new System.Windows.Forms.Label();
            this.lbl_Records_Received = new System.Windows.Forms.Label();
            this.lbl_ConnectionStatus = new System.Windows.Forms.Label();
            this.lbl_ConnectionStatus_Header = new System.Windows.Forms.Label();
            this.txt_Result = new System.Windows.Forms.TextBox();
            this.lbl_Records_Failed = new System.Windows.Forms.Label();
            this.ntfy_Icon = new System.Windows.Forms.NotifyIcon(this.components);
            this.cntxt_Main = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // lbl_Records_Sent
            // 
            this.lbl_Records_Sent.AutoSize = true;
            this.lbl_Records_Sent.Location = new System.Drawing.Point(34, 417);
            this.lbl_Records_Sent.Name = "lbl_Records_Sent";
            this.lbl_Records_Sent.Size = new System.Drawing.Size(75, 13);
            this.lbl_Records_Sent.TabIndex = 9;
            this.lbl_Records_Sent.Text = "Records Sent:";
            // 
            // lbl_Records_Received
            // 
            this.lbl_Records_Received.AutoSize = true;
            this.lbl_Records_Received.Location = new System.Drawing.Point(34, 399);
            this.lbl_Records_Received.Name = "lbl_Records_Received";
            this.lbl_Records_Received.Size = new System.Drawing.Size(99, 13);
            this.lbl_Records_Received.TabIndex = 8;
            this.lbl_Records_Received.Text = "Records Received:";
            // 
            // lbl_ConnectionStatus
            // 
            this.lbl_ConnectionStatus.AutoSize = true;
            this.lbl_ConnectionStatus.Location = new System.Drawing.Point(193, 382);
            this.lbl_ConnectionStatus.Name = "lbl_ConnectionStatus";
            this.lbl_ConnectionStatus.Size = new System.Drawing.Size(35, 13);
            this.lbl_ConnectionStatus.TabIndex = 7;
            this.lbl_ConnectionStatus.Text = "label1";
            // 
            // lbl_ConnectionStatus_Header
            // 
            this.lbl_ConnectionStatus_Header.AutoSize = true;
            this.lbl_ConnectionStatus_Header.Location = new System.Drawing.Point(31, 382);
            this.lbl_ConnectionStatus_Header.Name = "lbl_ConnectionStatus_Header";
            this.lbl_ConnectionStatus_Header.Size = new System.Drawing.Size(155, 13);
            this.lbl_ConnectionStatus_Header.TabIndex = 6;
            this.lbl_ConnectionStatus_Header.Text = "Websocket Connection Status:";
            // 
            // txt_Result
            // 
            this.txt_Result.Location = new System.Drawing.Point(30, 21);
            this.txt_Result.Multiline = true;
            this.txt_Result.Name = "txt_Result";
            this.txt_Result.Size = new System.Drawing.Size(740, 354);
            this.txt_Result.TabIndex = 5;
            // 
            // lbl_Records_Failed
            // 
            this.lbl_Records_Failed.AutoSize = true;
            this.lbl_Records_Failed.Location = new System.Drawing.Point(34, 433);
            this.lbl_Records_Failed.Name = "lbl_Records_Failed";
            this.lbl_Records_Failed.Size = new System.Drawing.Size(81, 13);
            this.lbl_Records_Failed.TabIndex = 10;
            this.lbl_Records_Failed.Text = "Records Failed:";
            // 
            // ntfy_Icon
            // 
            this.ntfy_Icon.BalloonTipText = "Listener is still running...";
            this.ntfy_Icon.ContextMenuStrip = this.cntxt_Main;
            this.ntfy_Icon.Icon = ((System.Drawing.Icon)(resources.GetObject("ntfy_Icon.Icon")));
            this.ntfy_Icon.Text = "TradeCapture Listener";
            this.ntfy_Icon.Visible = true;
            this.ntfy_Icon.DoubleClick += new System.EventHandler(this.ntfy_Icon_DoubleClick);
            // 
            // cntxt_Main
            // 
            this.cntxt_Main.Name = "cntxt_Main";
            this.cntxt_Main.Size = new System.Drawing.Size(61, 4);
            this.cntxt_Main.Text = "File";
            // 
            // frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbl_Records_Failed);
            this.Controls.Add(this.lbl_Records_Sent);
            this.Controls.Add(this.lbl_Records_Received);
            this.Controls.Add(this.lbl_ConnectionStatus);
            this.Controls.Add(this.lbl_ConnectionStatus_Header);
            this.Controls.Add(this.txt_Result);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "frm_Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "TradeCapture Listener";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_Main_FormClosed);
            this.Shown += new System.EventHandler(this.Form_Shown);
            this.Resize += new System.EventHandler(this.frm_Main_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Records_Sent;
        private System.Windows.Forms.Label lbl_Records_Received;
        private System.Windows.Forms.Label lbl_ConnectionStatus;
        private System.Windows.Forms.Label lbl_ConnectionStatus_Header;
        private System.Windows.Forms.TextBox txt_Result;
        private System.Windows.Forms.Label lbl_Records_Failed;
        private System.Windows.Forms.NotifyIcon ntfy_Icon;
        private System.Windows.Forms.ContextMenuStrip cntxt_Main;
    }
}