﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using System.Configuration;
using System.IO;
using WebSocketSharp;

namespace TradeCapture_Listener
{
    public partial class frm_Main : Form
    {
        private WebSocket _ws;
        private bool _connected;
        int messageReceiveCount = 0;
        int messageSentCount = 0;
        int messagedFailedCount = 0;
        private readonly System.Web.Script.Serialization.JavaScriptSerializer _jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        static System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();

        public frm_Main()
        {
            InitializeComponent();
            new Thread(StartWebsocketConnection).Start();

            //Build the menu controls for when form is minimized to tray
            var showMenuItem = cntxt_Main.Items.Add("Show");
            showMenuItem.Click += ntfy_Icon_DoubleClick;
            var exitMenuItem = cntxt_Main.Items.Add("Exit");
            exitMenuItem.Click += ExitMenu_Click;

            //Timer that will check the status of the websocket connection and try to reconnect if it's not active
            myTimer.Tick += new EventHandler(TimerEventProcessor);
            myTimer.Interval = (int)TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["DisconnectTimeoutIntervalSeconds"])).TotalMilliseconds;
            myTimer.Start();
        }

        //Actions to perform when a message is received from the websocket stream
        public void MessageReceived(object sender, MessageEventArgs message)
        {
            //increment the message counter
            messageReceiveCount++;

            //Build a sample of the connection feed ready message. Used to compare against and filter it out
            var payload = new Dictionary<string, string>
                    {
                        {"active", "true"},
                        {"description", "message feed ready" }
                    };
            var obj = new Dictionary<string, object>
                    {
                        {"eventName", "statusUpdate"},
                        {"payload", payload}
                    };
            var text = _jsSerializer.Serialize(obj);
            bool isStatusUpdate = false;

            try
            {
                if (!isStatusUpdate)
                {
                    //Grab the AWS details from the configuration file. The values are Base64 encoded, so they'll need to be decrypted
                    byte[] accessKey = Convert.FromBase64String(ConfigurationManager.AppSettings["AccessKey"]);
                    byte[] secretKey = Convert.FromBase64String(ConfigurationManager.AppSettings["AccessSecret"]);
                    byte[] streamName = Convert.FromBase64String(ConfigurationManager.AppSettings["KinesisFirehoseStreamName"]);
                    string accessKeyDecoded = Encoding.UTF8.GetString(accessKey);
                    string secretKeyDecoded = Encoding.UTF8.GetString(secretKey);
                    string streamNameDecoded = Encoding.UTF8.GetString(streamName);

                    //Initialize a Kinesis Firehose Client, build a record out of the message, and send it into the stream
                    AmazonKinesisFirehoseClient client = new AmazonKinesisFirehoseClient(Encoding.UTF8.GetString(accessKey), Encoding.UTF8.GetString(secretKey), GetEndpoint());
                    PutRecordRequest putRequest = new PutRecordRequest();
                    putRequest.DeliveryStreamName = Encoding.UTF8.GetString(streamName);
                    Record record = new Record();
                    record.Data = GenerateStreamFromString(message.Data + "\n");
                    putRequest.Record = record;
                    PutRecordResponse response = new PutRecordResponse();
                    response = client.PutRecord(putRequest);
                    //Check the response code from the client. If it was successful, incrememnt the successful message count, otherwise increment the failed count
                    if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                    {
                        messageSentCount++;
                    }
                    else
                    {
                        messagedFailedCount++;
                    }
                }
            }
            catch (Exception e)
            {
                //If anything failed here including connection, increment the failed count
                messagedFailedCount++;
            }

            if (text.Equals(message.Data, StringComparison.InvariantCultureIgnoreCase))
            {
                isStatusUpdate = true;
            }
            try
            {
                //Update the UI thread in case anyone is watching
                InvokeOnUiThread(() => AppendMessage(message.Data));

            }
            catch (Exception e)
            {
                //Implement additional Exception Handling here if needed
            }

        }

        public void Connected(object sender, EventArgs e)
        {
            //Once the websocket is connected, start the message feed and update the UI to show it is connected
            InvokeOnUiThread(() => UpdateStatus(true));
            SendStartFeedMessage();
        }

        public void Disconnected(object sender, CloseEventArgs e)
        {
            //If the client becomes disconnected, update the UI thread
            InvokeOnUiThread(() => UpdateStatus(false, e.Reason));
        }

        void _ws_OnError(object sender, WebSocketSharp.ErrorEventArgs e)
        {
            //If there are any websocket errors, update the UI thread
            InvokeOnUiThread(() => UpdateStatus(false, e.Message));
        }

        void StartWebsocketConnection()
        {
            try
            {
                if (_connected)
                {
                    return;
                }

                if (_ws != null)
                {
                    _ws.Close();
                }

                //Grab the websocket server connection URL from the config file and use it to establish the connection
                _ws = new WebSocket(ConfigurationManager.AppSettings["WebSocketServer"]);
                _ws.OnOpen += Connected;
                _ws.OnClose += Disconnected;
                _ws.OnError += _ws_OnError;
                _ws.OnMessage += MessageReceived;
                _ws.Connect();
            }
            catch (Exception e)
            {
                //Implement any error handling if needed
            }
        }

        public static Amazon.RegionEndpoint GetEndpoint()
        {
            //Mapping config file values to their object counterparts
            if (ConfigurationManager.AppSettings["AWSRegion"].ToLower() == "us-east-1")
            {
                return Amazon.RegionEndpoint.USEast1;
            }
            else if (ConfigurationManager.AppSettings["AWSRegion"].ToLower() == "us-west-1")
            {
                return Amazon.RegionEndpoint.USWest1;
            }
            else if (ConfigurationManager.AppSettings["AWSRegion"].ToLower() == "us-west-2")
            {
                return Amazon.RegionEndpoint.USWest2;
            }
            else if (ConfigurationManager.AppSettings["AWSRegion"].ToLower() == "us-east-2")
            {
                return Amazon.RegionEndpoint.USEast2;
            }
            else
            {
                return null;
            }
        }

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }

        private void frm_Main_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                //These 2 lines can be uncommented to show a nice little notification if desired, otherwise just hide the form when the window is minimized
                //ntfy_Icon.BalloonTipText = "Listener is still running...";
                //ntfy_Icon.ShowBalloonTip(500);
                this.Hide();
            }
        }

        private void ntfy_Icon_DoubleClick(object sender, EventArgs e)
        {
            //Bring the form to the front when the notification window is double clicked
            if (FormWindowState.Minimized == this.WindowState)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void ExitMenu_Click(object sender, EventArgs e)
        {
            //Exit the entire application...required since we're using background threads and don't want the process still running somewhere
            Environment.Exit(0);
        }

        private void frm_Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Exit the entire application...required since we're using background threads and don't want the process still running somewhere
            Environment.Exit(0);
        }

        //"Form Shown" event handler
        private void Form_Shown(object sender, EventArgs e)
        {
            //to minimize window
            this.WindowState = FormWindowState.Minimized;

            //to hide from taskbar
            this.Hide();
        }

        private void SendStartFeedMessage()
        {
            //This will be sent to the websocket client to initiate the handshake and start the feed
            var payload = new Dictionary<string, string>
                {
                    {"version", "1.0"}
                };
            var obj = new Dictionary<string, object>
                {
                    {"eventName", "startMessageFeed"},
                    {"payload", payload}
                };
            var text = _jsSerializer.Serialize(obj);
            _ws.Send(text);
        }

        void InvokeOnUiThread(Action action)
        {
            if (InvokeRequired)
            {
                BeginInvoke(action);
            }
            else
            {
                action.Invoke();
            }
        }

        private void UpdateStatus(bool connected, string message = "")
        {
            //Update the status message
            _connected = connected;

            var status = _connected ? "Connected" : "Disconnected";
            lbl_ConnectionStatus.Text = string.Format("{0} {1}", status, message);
        }

        void AppendMessage(string text)
        {
            //Update the last message box
            txt_Result.Text = text;
            lbl_Records_Failed.Text = "Messages logged (failed): " + messagedFailedCount.ToString();
            lbl_Records_Received.Text = "Messages Received: " + messageReceiveCount.ToString();
            lbl_Records_Sent.Text = "Messages logged (success): " + messageSentCount.ToString();
        }

        void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            //Check if the websocket is still connected, if not try to reconnect
            if (!_connected)
            {
                StartWebsocketConnection();
            }
        }
    }
}