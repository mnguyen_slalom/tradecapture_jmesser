from jinja2 import Environment, FileSystemLoader
import os
import string
import json
from datetime import datetime
 
root = os.path.dirname(os.path.abspath(__file__))
templates_dir = os.path.join(root, 'templates')
env = Environment( loader = FileSystemLoader(templates_dir) )
template = env.get_template('index.html')
templateTrade = env.get_template('trade.html')

def extract_time(json):
    try:
        # Also convert to int since update_time will be string.  When comparing
        # strings, "10" is smaller than "2".
        testing = int(datetime.strptime(json['data']['datetime'], "%Y-%m-%dT%H:%M:%S.%f%z").timestamp())
        return int(datetime.strptime(json['data']['datetime'], "%Y-%m-%dT%H:%M:%S.%f%z").timestamp())
    except KeyError:
        return 0

 
trade_list = [
{
  "trade_details": {
    "Contact_DisplayName": "GC - OB - Isaac",
    "Contact_ID": "Contact-15864742130",
    "Contact_Service": "ICE",
    "Contact_UserName": "icopeland",
    "COUNTERPARTY_COMPANY": "qt",
    "COUNTERPARTY_TRADER": "heinecke",
    "CYCLE_WINDOW": "c17",
    "FLOATING_EFP_score": 0.9768,
    "Message": "p66-Hightower sells 25mb c17 a2 to qt-heinecke at -1050 on a floating efp; efp price to be set as the april rbob settlement on friday March 15th; tyvm",
    "NONTRADE_score": 0.011,
    "pred": "FLOATING EFP",
    "pred_multileg": 0,
    "pred_pipeline": "NA",
    "PRODUCT": "a2",
    "PUMPOVER_score": 0.0048,
    "Reference_Line_Number": "151",
    "Sent/Received": "Received",
    "Time": "2019-03-14T15:28:03.626Z",
    "Trade_Detected": "0",
    "UserName": "ahightower1",
    "VOLUME": "25mb"
  },
  "trader": "ahightower1",
  "version_time": "v00_2019-03-14T15:28:03.626Z"
},
{
  "trade_details": {
    "Contact_DisplayName": "GC - Sage - Mark",
    "Contact_ID": "Contact-12165916002",
    "Contact_Service": "ICE",
    "Contact_UserName": "mmcconn",
    "COUNTERPARTY_COMPANY": "Trafi",
    "COUNTERPARTY_TRADER": "Eric",
    "CYCLE_WINDOW": "c17",
    "FLOATING_EFP_score": 0.9786,
    "INDEX_score": 0.0056,
    "Message": "P66; Tony sells Trafi; Eric c17 a2 x 25mb @ May RB -1050 on a Floating EFP to price vs tmrw 3/15 settle",
    "pred": "FLOATING EFP",
    "pred_multileg": 0,
    "pred_pipeline": "NA",
    "pred_trade_details": {
      "pred_counterparty": "trafi; eric",
      "pred_cycles": "c17",
      "pred_price": {
        "Context": "a2 x 25mb @ may rb -1050 on a floating efp to price vs tmrw 3/15 settle",
        "Contract_Exch": "NA",
        "Contract_Month": "may",
        "Contract_Type": "rb",
        "Differential": " -1050 ",
        "Settle_Date": "TBD"
      },
      "pred_product": "a2",
      "pred_quantity": "25mb"
    },
    "PRODUCT": "a2",
    "Reference_Line_Number": "147",
    "Sent/Received": "Received",
    "Time": "2019-03-14T15:25:28.593Z",
    "Trade_Detected": "0",
    "TRIGGER_score": 0.0037,
    "UserName": "ahightower1",
    "VOLUME": "25mb"
  },
  "trader": "ahightower1",
  "version_time": "v00_2019-03-14T15:25:28.593Z"
},
{
  "trade_details": {
    "Contact_DisplayName": "UET_Zack Hildebrandt",
    "Contact_ID": "Contact-19316496052",
    "Contact_Service": "ICE",
    "Contact_UserName": "zhildebrandt",
    "COUNTERPARTY_COMPANY": "UET",
    "COUNTERPARTY_TRADER": "Zac Hildebrandt",
    "INDEX_score": 0.9784,
    "Message": "P66  (Taylor Rife) Sells UET (Zac Hildebrandt) 50mb/month;  62 grade; on 12 month strip (5/1/2019-4/30/2020); FOB Magellan E. Houston Tankage. Option to increase monthly volume to 75mb upon mutual agreement between parties. Nominations to be sent and received no later than the 15th day of prior month.   To be Priced at on Argus 62gr Pipeline Outright Mean; 3 day wrap;  plus a premium of 185 pts.  Exact pricing dates to be mutually agreed upon.",
    "NONTRADE_score": 0.0123,
    "pred": "INDEX",
    "pred_multileg": 1,
    "pred_pipeline": "Magellan",
    "PRODUCT": ['62 grade', '62gr'],
    "PUMPOVER_score": 0.0046,
    "Reference_Line_Number": "9",
    "Sent/Received": "Received",
    "Time": "2019-03-14T20:46:07.460Z",
    "Trade_Detected": "0",
    "UserName": "trife",
    "VOLUME": ['50mb', '75mb']
  },
  "trader": "trife",
  "version_time": "v00_2019-03-14T20:46:07.460Z"
}
]

filename = os.path.join(root, 'html', 'index.html')
with open(filename, 'w', encoding="utf-8") as fh:
    colonialTrades = []
    nonColonialTrades = []
    for trade in trade_list:
        trade_id = trade['trader'] + "_" + trade['version_time'].replace('#','_').replace(':','').replace('.','')
        trade['trade_id'] = trade_id
        colonialTrades.append(trade)
        #if trade['data']['via'].lower() == "colonial":
            #colonialTrades.append(trade)
        #else:
            #nonColonialTrades.append(trade)
    #fh.write(template.render(
        #colonialItems = colonialTrades,
        #nonColonialItems = nonColonialTrades
    #))
    colonialTrades.sort(key=extract_time, reverse=True)
    fh.write(template.render(
        colonialItems = colonialTrades
    ))

for trade in trade_list:
    if not os.path.exists(os.path.join(root, 'html', 'trades')):
        os.makedirs(os.path.join(root, 'html', 'trades'))
    trade_id = trade['trader'] + "_" + trade['version_time'].replace('#','_').replace(':','').replace('.','')
    trade['trade_id'] = trade_id
    tradeFileName = os.path.join(root, 'html', 'trades', trade_id + ".html")
    with open(tradeFileName, 'w', encoding='utf-8') as fhTrades:
        fhTrades.write(templateTrade.render(
            tradeItem = trade
        ))