# Trade Capture

The Phillips 66 Commercial Business Unit executes trades and deals with third parties over ICE instant message, Email, and Voice Calls. At the end of the trade day, traders must manually record their deals into the Energy Trade and Risk Management (ETRM) system (currently RightAngle, to be replaced by SAP S4) before the 5pm deadline to meet trade confirmation deadlines. This process is time consuming and cumbersome and requires traders to keep track of details over the course of the day for end of day data entry.
Phillips 66 would like to execute a proof of concept (POC) to test using machine learning solutions to replace manual deal entry into RightAngle.  Although traders utilize email and voice calls to do business, the majority of trading activity is conducted via IM, and this POC focuses only on IM communications.

## Architecture
![Architecture](Documentation/Images/Architecture.png)

## Machine Learning Workflow
![ML](Documentation/Images/ML_Workflow_Sprint2.png)

## Lambda Processing Flow
![Lambda](Documentation/Images/AWS_Processing_Flow_Current.png)

## Repository
Type | Name  |
------------- |------------- |
Application | ICE IM API Listener  |
Lambda |tradecaptures3dailylogcombiner|
Lambda |tradecapturemsgclassifierlambda|
Lambda |tradecapturedataextract|
Lambda |tradecaptureentityextract|
Lambda |tradecaptureentitiesdynamo|
Lambda |tradecapturehtmlgenerator|
  